### Contract tests
To run the tests, you have to run the service on port 8000. Than you can run the tests by ```python src/contract_tests.py```.

For the contract testing, I am using ```pact-python``` framework - https://github.com/pact-foundation/pact-python .