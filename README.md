### Endpoint
The endpoint is located at ```/send_email/``` - POST and you have to call it with body ```{"recipient":"test", "subject": "test", "message": "test"}```

The application is running in docker - you have to run ```docker-compose up``` and the app is running on port ```8000```.

There is also validation if request contains ```recipient, subject, message``` attributes in JSON and if the email address is valid.

For sending emails, I am using the https://ethereal.email/ - ```dianna99@ethereal.email``` and ```UZGhEZh6Arr8RHH56t``` .