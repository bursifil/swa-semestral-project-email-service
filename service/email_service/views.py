import json
import smtplib
import re
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

RECIPIENT_KEY = "recipient"
MESSAGE_KEY = "message"
SUBJECT_KEY = "subject"
SENDER_PASSWORD = "UZGhEZh6Arr8RHH56t"
SENDER_EMAIL = "dianna99@ethereal.email"
SMTP_ADDRESS = "smtp.ethereal.email: 587"

regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'


@csrf_exempt
def send_email(request):
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')

        try:
            body = json.loads(body_unicode)
        except:
            return JsonResponse({'error': 'sent invalid JSON'}, status=400)

        if RECIPIENT_KEY in body:
            recipient = body[RECIPIENT_KEY]
        else:
            return JsonResponse({'error': 'recipient is a required attribute'}, status=400)

        if MESSAGE_KEY in body:
            message = body[MESSAGE_KEY]
        else:
            return JsonResponse({'error': 'message is a required attribute'}, status=400)

        if SUBJECT_KEY in body:
            subject = body[SUBJECT_KEY]
        else:
            return JsonResponse({'error': 'subject is a required attribute'}, status=400)

        if not (check_email_address(recipient)):
            return JsonResponse({'error': 'invalid email address of recipient'}, status=400)

        msg = MIMEMultipart()
        message = message

        msg['From'] = SENDER_EMAIL
        msg['To'] = recipient
        msg['Subject'] = subject

        msg.attach(MIMEText(message, 'plain'))

        server = smtplib.SMTP(SMTP_ADDRESS)
        server.starttls()
        server.login(msg['From'], SENDER_PASSWORD)
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.quit()

        return HttpResponse(status=204)
    else:
        return JsonResponse({'error': 'you have to call POST endpoint'}, status=400)


def show_default(request):
    return HttpResponse("OK")


def check_email_address(email):
    if re.search(regex, email):
        return True
    else:
        return False
